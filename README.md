# UnderTest.FeatureLint

Linter for gherkin feature files.

## Getting Started

1. Install the global tool via `dotnet tool install -g UnderTest.FeatureLint`
1. Invoke `featurelint *.feature` 
1. Any issues or errors will be outputted
