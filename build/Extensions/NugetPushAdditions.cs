using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using NuGet;
using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Tooling;

// todo -  move to use UnderTest.Nuke when published to nuget.
// Extension methods go in the same namespace as their extending type
// ReSharper disable once CheckNamespace
namespace UnderTest.Nuke.Tools
{
  public static class NugetPushAdditions
  {
    public static IReadOnlyCollection<Output> NugetPushIfVersionDoesNotExistOnNugetServer(string filename,
      Func<string, IReadOnlyCollection<Output>> nugetPushFunc)
    {
      var version = GetNuGetPackageVersion(filename);
      if (NugetPackageVersionExists(GetPackageId(filename), version.ToOriginalString()))
      {
        Logger.Warn("Package not being pushed as it exists on Nuget.org");
        return null;
      }

      return nugetPushFunc(filename);
    }

    public static bool NugetPackageVersionExists(string packageIdP, string versionP)
    {
      try
      {
        var url = $"https://api-v2v3search-0.nuget.org/query?q=packageid:{packageIdP}&prerelease=true";

        var response = HttpTasks.HttpDownloadString(url, requestConfigurator: x => x.Timeout = int.MaxValue);
        var packageObject = JsonConvert.DeserializeObject<JObject>(response);

        var versions = packageObject["data"].Single()["versions"].ToArray();

        return versions.ToList().Exists(x => x["version"].ToString() == versionP);
      }
      catch
      {
        Logger.Warn($"Unable to lookup {packageIdP} on nuget.org for existence.");
        return false;
      }
    }

    private static SemanticVersion GetNuGetPackageVersion(string filename)
    {
      var p = new ZipPackage(filename);

      return p.Version;
    }

    private static string GetPackageId(string filename)
    {
      var p = new ZipPackage(filename);

      return p.Id;
    }
  }
}
