using System;
using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Tests.Testables;

namespace UnderTest.FeatureLint.Common.Tests
{
  public class FeatureLintRunnerTests
  {
    [Test]
    public void Constructor_WhenCalled_NoExceptions()
    {
      var config = new TestableFeatureOptions { FilePaths = new List<string>()};
      var instance = new FeatureLintRunner(config);

      instance.Should().NotBeNull();
    }

    [Test]
    public void Execute_WhenPassedNullFiles_ThrowsNullArgumentException()
    {
      var config = new TestableFeatureOptions { FilePaths = null };
      var instance = new FeatureLintRunner(config);
      Action act = () => instance.Execute();

      act.Should().Throw<NullReferenceException>();
    }
  }
}
