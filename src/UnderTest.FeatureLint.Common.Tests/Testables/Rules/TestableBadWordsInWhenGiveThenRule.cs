using System.Diagnostics.CodeAnalysis;
using Gherkin.Ast;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Testables.Rules
{
  public class TestableBadWordsInWhenGiveThenRule : BadWordsInWhenGiveThenRule
  {
    [ExcludeFromCodeCoverage]
    public bool PublicStepContainsBadWord(Step stepP, out string foundWord)
    {
      return base.StepContainsBadWord(stepP, out foundWord);
    }
  }
}
