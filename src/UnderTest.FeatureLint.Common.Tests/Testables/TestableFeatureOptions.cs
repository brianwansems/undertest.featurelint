using System.Collections.Generic;

namespace UnderTest.FeatureLint.Common.Tests.Testables
{
  public class TestableFeatureOptions : IFeatureLintConfig
  {
    public IList<string> FilePaths   { get; set; }
  }
}
