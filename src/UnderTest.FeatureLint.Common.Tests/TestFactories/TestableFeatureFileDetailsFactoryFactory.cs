using UnderTest.FeatureLint.Common.Tests.Testables;

namespace UnderTest.FeatureLint.Common.Tests.TestFactories
{
  public class TestableFeatureFileDetailsFactoryFactory
  {
    const string ReturnedFilename = "testable-class-returns-contents.feature";

    public FeatureFileDetails EmptyFile()
    {
      return BuildResult(string.Empty);
    }

    public FeatureFileDetails SimpleTestFileWithNoSteps()
    {
      const string featureContent = "Feature: test feature";

      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithSimpleValidSteps(string name = "test feature")
    {
      var featureContent = $@"
Feature: {name}

Scenario: simple
  Give all of this is true
  When something happens
  Then these new things are true
";

      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithSimpleValidStepsButNoFeatureTitle()
    {
      const string featureContent = @"
Feature:

Scenario: simple
  Give all of this is true
  When something happens
  Then these new things are true
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithSimpleValidStepsButNoScenarioTitle()
    {
      const string featureContent = @"
Feature: simple

Scenario:
  Give all of this is true
  When something happens
  Then these new things are true
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithABackgroundWithoutATitle()
    {
      const string featureContent = @"
Feature: Feature with a Background

Background:
  Given a feature file

Scenario Outline: Feature file has a background with no name
  Given the feature file contains a background
    And the background <does?> have a name
  When I lint the feature file
  Then I <should?> be warned about a scenario with no name
  Examples: Background does and does not have a name
    | does?    | should?    |
    | does     | should not |
    | does not | should     |
";

      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioOutlineWithButNoExamples()
    {
      const string featureContent = @"
Feature: simple

Scenario Outline:
  Give all of this is true
  When something happens
  Then these new things are true
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioOutlineWithNoExampleName()
    {
      const string featureContent = @"
Feature: simple

Scenario Outline:
  Give all of this is true
  When something happens
  Then these new things are true
  Examples:
     | a |
     | 1 |
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithBadWords()
    {
      const string featureContent = @"
Feature: test feature

Scenario: bad words
  Given all of this may be true
  When something wants to happens
  Then these new things might be true
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithBadWordsAsTheLastWordOnTheLine()
    {
      const string featureContent = @"
Feature: test feature

Scenario: bad words
  Given all of this be true
  When something happens
  Then these new things are true and
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithInvalidOrder()
    {
      const string featureContent = @"
Feature: test feature

Scenario: bad order
  When this
    And dis
  Then that
  Given another thing
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithDuplicateKeywords()
    {
      const string featureContent = @"
Feature: test feature

Scenario: bad order
  When this
  When dis
  Then that
  Given another thing
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithDuplicateTagsOnFeature()
    {
      const string featureContent = @"
@tag @tag
Feature: dup tags on feature

Scenario: basic scenario
  When this
  When dis
  Then that
  Given another thing
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithDuplicateTagsOnScenario()
    {
      const string featureContent = @"
Feature: test feature

@tag @tag
Scenario: dup tags on scenario
  When this
  When dis
  Then that
  Given another thing
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithDuplicateTagsOnScenarioExample()
    {
      const string featureContent = @"
Feature: test feature

Scenario Outline: dup tags on example
  When this
  When dis
  Then that
  Given another thing <something>
@tag @tag
Examples:
  | something |
  | a         |
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails FeatureNameEndsInPeriod()
    {
      const string featureContent = @"
Feature: test feature.

Scenario: cool stuff
  Given another thing
  When this
  Then that
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails BackgroundContainsThen()
    {
      const string featureContent = @"
Feature: test feature.

Background:
  Given foo
  When bar
  Then foobar

Scenario: cool stuff
  Given another thing
  When this
  Then that
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails ScenarioNameEndsInPeriod()
    {
      const string featureContent = @"
Feature: test feature

Scenario: cool stuff.
  Given another thing
  When this
  Then that
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails StepNameEndsInPeriod()
    {
      const string featureContent = @"
Feature: test feature

Scenario: cool stuff
  Given another thing.
  When this
  Then that
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails TwoScenariosWithDuplicateNames()
    {
      const string featureContent = @"
Feature: test feature

Scenario: cool stuff
  Given another thing
  When this
  Then that

Scenario: cool stuff
  Given thing
  When this
  Then that
";
      return BuildResult(featureContent);
    }

    public FeatureFileDetails OneScenarioWithUpperCaseWord()
    {
      const string featureContent = @"
Feature: test feature

Scenario: bad words
  Given all of this may be true
  When something wants to happens
  Then these new things might BE true
";
      return BuildResult(featureContent);
    }

    private static FeatureFileDetails BuildResult(string featureContent)
    {
      var instance = new TestableFeatureFileDetailsFactory
      {
        FeatureContent = featureContent
      };

      return instance.GetInstance(ReturnedFilename);
    }
  }
}
