using System;
using FluentAssertions;
using Gherkin;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Tests.Testables;

namespace UnderTest.FeatureLint.Common.Tests
{
  public class FeatureFileDetailsFactoryTests
  {
    [Test]
    public void Constructor_WhenCalled_CreatesInstance()
    {
      var instance = new FeatureFileDetailsFactory();

      instance.Should().NotBeNull();
    }

    [Test]
    public void GetInstance_WhenPassedNull_ThrowsArgumentNullException()
    {
      var instance = new FeatureFileDetailsFactory();

      Action act = () => instance.GetInstance(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void GetInstance_WhenPassedValidFile_ParsesAndReturns()
    {
      const string filename = "testable-class-returns-contents.feature";
      const string featureContent = "Feature: test feature";
      var instance = new TestableFeatureFileDetailsFactory
      {
        FeatureContent = featureContent
      };

      var result = instance.GetInstance(filename);

      result.Gherkin.Feature.Name.Should().Be("test feature");
      result.Contents.Should().Be(featureContent);
      result.FilePath.Should().Be(filename);
    }

    [Test]
    public void GetInstance_WhenPassedInvalidFile_ThrowsException()
    {
      const string filename = "testable-class-returns-contents.feature";
      const string featureContent = "NOT A VALID FEATURE";
      var instance = new TestableFeatureFileDetailsFactory
      {
        FeatureContent = featureContent
      };

      Action act = () => instance.GetInstance(filename);

      act.Should().Throw<ParserException>();
    }
  }
}
