using FluentAssertions;
using NUnit.Framework;

namespace UnderTest.FeatureLint.Common.Tests
{
  public class FeatureLintResultTests
  {
    [Test]
    public void Constructor_WhenCalled_DefaultsProperties()
    {
      var instance = new FeatureLintResult();

      instance.Findings.Should().NotBeNull();
      instance.Findings.Count.Should().Be(0);
    }
  }
}
