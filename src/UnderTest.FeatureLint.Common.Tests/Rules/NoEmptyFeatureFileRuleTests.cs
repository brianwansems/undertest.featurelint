using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class NoEmptyFeatureFileRuleTests : BaseRuleTests<NoEmptyFeatureFileRule>
  {
    [Test]
    public override void Run_WhenPassedEmptyFile_ReturnsResults()
    {
      var instance = new NoEmptyFeatureFileRule();
      var contents = m_MetaFactory.EmptyFile();

      var results = instance.Run(contents);

      results.Count.Should().Be(1);
      var result = results.First();
      result.Type.Should().Be(FeatureLintDataType.Error);
      result.Line.Should().Be(0);
      result.Column.Should().Be(0);
      result.Message.Should().Be(NoEmptyFeatureFileRule.NoFeatureTitleMessage);
    }
  }
}
