using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class ScenarioOutlineShouldHaveAnExampleTableRuleTests : BaseRuleTests<ScenarioOutlineShouldHaveAnExampleTableRule>
  {
    [Test]
    public void Run_WhenPassedScenarioWithNoExamples_ReturnsAnError()
    {
      var instance = new ScenarioOutlineShouldHaveAnExampleTableRule();
      var contents = m_MetaFactory.OneScenarioOutlineWithButNoExamples();

      var results = instance.Run(contents);

      results.Count.Should().Be(1);
      var result = results.First();
      result.Line.Should().Be(4);
      result.Column.Should().Be(1);
      result.Message.Should().Be(ScenarioOutlineShouldHaveAnExampleTableRule.ScenarioOutlineShouldHaveExampleMessage);
    }
  }
}
