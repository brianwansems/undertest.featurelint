using System;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;
using UnderTest.FeatureLint.Common.Tests.TestFactories;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public abstract class BaseRuleTests<TypeUnderTest> where TypeUnderTest: ILintRule, new()
  {
    [SetUp]
    public void Setup()
    {
      m_MetaFactory = new TestableFeatureFileDetailsFactoryFactory();
    }

    protected TestableFeatureFileDetailsFactoryFactory m_MetaFactory;

    [Test]
    public virtual void WhenCreated_Order_ShouldBeNull()
    {
      var instance = new TypeUnderTest();

      instance.Order.Should().BeNull();
    }

    [Test]
    public void Run_WhenPassedNull_ShouldThrow()
    {
      var instance = new TypeUnderTest();

      Action act = () => instance.Run(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void Run_WhenPassedSimpleValidFile_ReturnsNoResults()
    {
      var instance = new TypeUnderTest();
      var contents = m_MetaFactory.SimpleTestFileWithNoSteps();

      var result = instance.Run(contents);

      result.Count.Should().Be(0);
    }

    [Test]
    public virtual void Run_WhenPassedEmptyFile_ReturnsResults()
    {
      var instance = new TypeUnderTest();
      var contents = m_MetaFactory.EmptyFile();

      var result = instance.Run(contents);

      result.Count.Should().Be(0);
    }
  }
}
