using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class BadWordsInWhenGiveThenRuleTests: BaseRuleTests<BadWordsInWhenGiveThenRule>
  {
    [Test]
    public void Run_WhenPassedValidFileWithBadWords_ReturnsBadWords()
    {
      var instance = new BadWordsInWhenGiveThenRule();
      var contents = m_MetaFactory.OneScenarioWithBadWords();

      var result = instance.Run(contents);

      result.Count.Should().Be(3);
    }

    [Test]
    public void Run_WhenPassedValidFileWithBadWordsThatAreTheLastWordOnTheLine_ReturnsBadWords()
    {
      var instance = new BadWordsInWhenGiveThenRule();
      var contents = m_MetaFactory.OneScenarioWithBadWordsAsTheLastWordOnTheLine();

      var result = instance.Run(contents);

      result.Count.Should().Be(1);
    }
  }
}
