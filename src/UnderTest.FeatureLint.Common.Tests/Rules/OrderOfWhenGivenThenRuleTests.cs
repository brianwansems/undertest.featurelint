using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class OrderOfWhenGivenThenRuleTests : BaseRuleTests<OrderOfWhenGiveThenRule>
  {
    [Test]
    public void Run_WhenPassedInvalidOrderGherkin_ReturnsErrorsForEachItemOutOfOrder()
    {
      var instance = new OrderOfWhenGiveThenRule();
      var contents = m_MetaFactory.OneScenarioWithInvalidOrder();

      var result = instance.Run(contents);

      result.Count.Should().Be(1);
      var found = result.First();
      found.Type.Should().Be(FeatureLintDataType.Error);
      found.Message.Should().Be("Given out of order. Given, When and Then must be used in order");
      found.Line.Should().Be(8);
    }
  }
}
