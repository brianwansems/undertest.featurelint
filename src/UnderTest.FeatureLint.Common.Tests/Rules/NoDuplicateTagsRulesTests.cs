using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class NoDuplicateTagsRulesTests : BaseRuleTests<NoDuplicateWhenGivenThenRule>
  {
    [Test]
    public void Run_WhenPassedDuplicateTagsOnFeature_ReturnsErrorForEachTag()
    {
      var instance = new NoDuplicateTagsRule();
      var contents = m_MetaFactory.OneScenarioWithDuplicateTagsOnFeature();

      var result = instance.Run(contents);

      result.Count.Should().Be(1);
      var found = result.First();
      found.Type.Should().Be(FeatureLintDataType.Warning);
      found.Line.Should().Be(2);
      found.Message.Should()
        .Be("Duplicate tag '@tag' found 2 times.");
    }

    [Test]
    public void Run_WhenPassedDuplicateTagsOnScenario_ReturnsErrorForEachTag()
    {
      var instance = new NoDuplicateTagsRule();
      var contents = m_MetaFactory.OneScenarioWithDuplicateTagsOnScenario();

      var result = instance.Run(contents);

      result.Count.Should().Be(1);
      var found = result.First();
      found.Type.Should().Be(FeatureLintDataType.Warning);
      found.Line.Should().Be(4);
      found.Message.Should()
        .Be("Duplicate tag '@tag' found 2 times.");
    }

    [Test]
    public void Run_WhenPassedDuplicateTagsOnExamples_ReturnsErrorForEachTag()
    {
      var instance = new NoDuplicateTagsRule();
      var contents = m_MetaFactory.OneScenarioWithDuplicateTagsOnScenarioExample();

      var result = instance.Run(contents);

      result.Count.Should().Be(1);
      var found = result.First();
      found.Type.Should().Be(FeatureLintDataType.Warning);
      found.Line.Should().Be(9);
      found.Message.Should()
        .Be("Duplicate tag '@tag' found 2 times.");
    }
  }
}
