using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class NoDuplicateWhenGiveThenRuleTests : BaseRuleTests<NoDuplicateWhenGivenThenRule>
  {
    [Test]
    public void Run_WhenPassedDuplicateStepNames_ReturnsErrorForEachKeyword()
    {
      var instance = new NoDuplicateWhenGivenThenRule();
      var contents = m_MetaFactory.OneScenarioWithDuplicateKeywords();

      var result = instance.Run(contents);

      result.Count().Should().Be(1);
      var found = result.First();
      found.Type.Should().Be(FeatureLintDataType.Error);
      found.Line.Should().Be(5);
      found.Message.Should()
        .Be("Duplicate When keyword found. The `And` keyword should be used when duplicates are found.");
    }
  }
}
