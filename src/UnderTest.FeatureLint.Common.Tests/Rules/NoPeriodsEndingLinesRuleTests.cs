using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class NoPeriodsEndingLinesRuleTests : BaseRuleTests<NoPeriodsEndingLinesRule>
  {
    [Test]
    public void Run_FeatureEndsInPeriod_ReturnsAnError()
    {
      var instance = new NoPeriodsEndingLinesRule();
      var content = m_MetaFactory.FeatureNameEndsInPeriod();

      var results = instance.Run(content);

      results.Count.Should().Be(1);

      var result = results.First();
      result.Line.Should().Be(2);
      result.Column.Should().Be(1);
      result.Type.Should().Be(FeatureLintDataType.Warning);
      result.Message.Should().Be("Feature name should not end in a period");
    }

    [Test]
    public void Run_ScenarioNameEndsInPeriod_ReturnsAnError()
    {
      var instance = new NoPeriodsEndingLinesRule();
      var content = m_MetaFactory.ScenarioNameEndsInPeriod();

      var results = instance.Run(content);

      results.Count.Should().Be(1);

      var result = results.First();
      result.Line.Should().Be(4);
      result.Column.Should().Be(1);
      result.Type.Should().Be(FeatureLintDataType.Warning);
      result.Message.Should().Be("Scenario name should not end in a period");
    }

    [Test]
    public void Run_StepNameEndsInPeriod_ReturnsAnError()
    {
      var instance = new NoPeriodsEndingLinesRule();
      var content = m_MetaFactory.StepNameEndsInPeriod();

      var results = instance.Run(content);

      results.Count.Should().Be(1);

      var result = results.First();
      result.Line.Should().Be(5);
      result.Column.Should().Be(3);
      result.Type.Should().Be(FeatureLintDataType.Warning);
      result.Message.Should().Be("Step should not end in a period");
    }
  }
}
