using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class NoUpperCaseWordsRuleTests : BaseRuleTests<NoUpperCaseWordsRule>
  {
    [Test]
    public void Run_WhenPassedValidFileWithUpperCaseWords_ReturnsUpperCaseWords()
    {
      var instance = new NoUpperCaseWordsRule();
      var contents = m_MetaFactory.OneScenarioWithUpperCaseWord();

      var result = instance.Run(contents);

      result.Count.Should().Be(1);
    }
  }
}
