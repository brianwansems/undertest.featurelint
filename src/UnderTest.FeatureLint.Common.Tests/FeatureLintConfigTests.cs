using System;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Tests.Testables;

namespace UnderTest.FeatureLint.Common.Tests
{
  public class FeatureLintConfigTests
  {
    [Test]
    public void Constructor_PassedNull_ThrowsException()
    {
      Action act = () => new FeatureLintConfig(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void Constructor_PassedOptions_SetsProperties()
    {
      var filepaths = new string[] { "test.feature" };
      var options = new TestableFeatureOptions { FilePaths = filepaths };

      var instance = new FeatureLintConfig(options);

      instance.FilePaths.Should().Equal(filepaths);
    }
  }
}
