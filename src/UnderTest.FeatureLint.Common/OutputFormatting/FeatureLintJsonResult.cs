using System.Collections.Generic;

namespace UnderTest.FeatureLint.Common.OutputFormatting
{
  public class FeatureLintJsonResult
  {
    public bool Successful { get; set; }

    public IList<FeatureLintResultGroupedByFile> Results { get; set; } = new List<FeatureLintResultGroupedByFile>();
  }
}