using System;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;

namespace UnderTest.FeatureLint.Common.OutputFormatting
{
  public interface IOutputFormatter
  {
    string BuildErrorResult(FeatureLintResult result);

    string BuildSuccessResult();
  }
}
