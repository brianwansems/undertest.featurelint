namespace UnderTest.FeatureLint.Common
{
  public class InMemoryFeatureFileDetailsFactory : FeatureFileDetailsFactory
  {
    public string FeatureContent { get; set; }

    public FeatureFileDetails GetInstance()
    {
      return GetInstance("InMemoryFeatureFileDetails");
    }

    protected override string ReadContent(string filePathP)
    {
      return FeatureContent;
    }
  }
}
