using System.Collections.Generic;

namespace UnderTest.FeatureLint.Common
{
  public class FeatureLintResult
  {
    public List<FeatureLintData> Findings { get; } = new List<FeatureLintData>();

    public FeatureLintResultSummary Summary { get; } = new FeatureLintResultSummary();
  }
}
