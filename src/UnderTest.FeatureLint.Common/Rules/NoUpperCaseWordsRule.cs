using System;
using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class NoUpperCaseWordsRule : ILintRule
  {
    public int? Order { get; } = null;

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      if (contentsP == null)
      {
        throw new ArgumentNullException(nameof(contentsP));
      }

      var result = new List<FeatureLintData>();
      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      foreach (var scenario in doc.Feature.Children.GatherAllStepsContainers())
      {
        foreach (var step in scenario.Steps)
        {
          if (StepContainsUpperCaseWord(step, out string foundWord))
          {
            result.Add(new FeatureLintData
            {
              Line = step.Location.Line,
              Column = step.Location.Column,
              FilePath = contentsP.FilePath,
              Type = FeatureLintDataType.Warning,
              Message = $"Upper case word found '{foundWord.Trim()}' - on line '{step.Text}', ensure this is an acronym"
            });
          }
        }
      }
      return result;
    }

    protected virtual bool StepContainsUpperCaseWord(Step stepP, out string foundWord)
    {
      foundWord = null;

      //todo - ability to maintain acronym lists

      var text = stepP.Text;
      var punctuation = text.Where(char.IsPunctuation).Distinct().ToArray();
      var words = text.Split().Select(x => x.Trim(punctuation)).ToList();

      foreach (var word in words)
      {
        if (word.All(char.IsUpper))
        {
          foundWord = word;
          return true;
        }
      }
      return false;
    }

  }
}
