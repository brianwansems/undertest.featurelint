using System;
using System.Collections.Generic;
using Gherkin.Ast;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class OrderOfWhenGiveThenRule : ILintRule
  {
    public int? Order { get; } = null;

    public IList<FeatureLintData> Run(FeatureFileDetails contentsP)
    {
      if (contentsP == null)
      {
        throw new ArgumentNullException(nameof(contentsP));
      }

      const string whenKeyword = "When";
      const string givenKeyword = "Given";
      const string thenKeyword = "Then";
      var result = new List<FeatureLintData>();

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      foreach (var scenario in doc.Feature.Children.GatherAllStepsContainers())
      {
        var seenGiven = false;
        var seenWhen = false;
        var seenThen = false;
        var lastKeyword = "";
        foreach (var step in scenario.Steps)
        {
          var keyword = step.Keyword.Trim();
          switch (keyword)
          {
            case givenKeyword:
            case "And" when lastKeyword == givenKeyword:
            {
              lastKeyword = givenKeyword;
              seenGiven = true;
              if (seenThen || seenWhen)
              {
                result.Add(MapToFeatureLintData(contentsP, step, givenKeyword));
              }

              break;
            }

            case whenKeyword:
            case "And" when lastKeyword == whenKeyword:
            {
              lastKeyword = whenKeyword;
              seenWhen = true;
              if (seenThen)
              {
                result.Add(MapToFeatureLintData(contentsP, step, whenKeyword));
              }

              break;
            }

            case thenKeyword:
            case "And" when lastKeyword == thenKeyword:
            {
              lastKeyword = thenKeyword;
              seenThen = true;
              if (!seenGiven && !seenWhen)
              {
                result.Add(MapToFeatureLintData(contentsP, step, thenKeyword));
              }

              break;
            }
          }
        }
      }

      return result;
    }

    private static FeatureLintData MapToFeatureLintData(FeatureFileDetails contentsP, Step step, string keyword)
    {
      return new FeatureLintData
      {
        Line = step.Location.Line,
        Column = step.Location.Column,
        FilePath = contentsP.FilePath,
        Message = $"{keyword} out of order. Given, When and Then must be used in order",
        Type = FeatureLintDataType.Error
      };
    }
  }
}
