using System;
using System.Collections.Generic;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class ScenarioOutlineExamplesShouldHaveANameRule : ILintRule
  {
    public static string ScenarioOutlineExamplesShouldHaveANameMessage = "Examples should have a name";

    public int? Order { get; } = null;

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      if (contentsP == null)
      {
        throw new ArgumentNullException(nameof(contentsP));
      }

      var result = new List<FeatureLintData>();

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      foreach (var scenario in doc.Feature.Children.GatherAllScenarios())
      {
        foreach (var example in scenario.Examples)
        {
          if (string.IsNullOrWhiteSpace(example.Name))
          {
            result.Add(new FeatureLintData
            {
              Line = example.Location.Line,
              Column = example.Location.Column,
              FilePath = contentsP.FilePath,
              Type = FeatureLintDataType.Error,
              Message = ScenarioOutlineExamplesShouldHaveANameMessage
            });
          }
        }
      }

      return result;
    }
  }
}
