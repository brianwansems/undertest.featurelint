using System.Collections.Generic;
using System.IO;
using Gherkin;
using Gherkin.Ast;

namespace UnderTest.FeatureLint.Common.Rules
{
  public interface ILintRule
  {
    int? Order { get; }

    IList<FeatureLintData> Run(FeatureFileDetails contentsP);
  }
}
