using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class EnsureFeatureNamesAreUniqueRule : ILintRule
  {
    public int? Order { get; } = null;

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      if (contentsP == null)
      {
        throw new ArgumentNullException(nameof(contentsP));
      }

      var result = new List<FeatureLintData>();

      if (contentsP.Gherkin.Feature == null)
      {
        return result;
      }

      var feature = contentsP.Gherkin.Feature;
      var duplicated_scenarios = contentsP.OtherValidFiles
                                  .Where(x => x.Gherkin.Feature != null && string.Equals(
                                    x.Gherkin.Feature.Name,
                                    feature.Name,
                                    StringComparison.InvariantCultureIgnoreCase))
                                  .ToList();

      result.AddRange(duplicated_scenarios
        .Select(x =>
          new FeatureLintData
          {
            Line = feature.Location.Line,
            Column = feature.Location.Column,
            Type = FeatureLintDataType.Error,
            Message = $@"Duplicate feature name with name ""{feature.Name}"")",
            FilePath = contentsP.FilePath
          }));

      return result;
    }
  }
}
