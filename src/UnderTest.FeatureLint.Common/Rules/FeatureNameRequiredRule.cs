using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class FeatureNameRequiredRule : ILintRule
  {
    public int? Order { get; } = null;

    public static string NoFeatureTitleMessage = "Feature must have a non-empty name";

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      if (contentsP == null)
      {
        throw new ArgumentNullException(nameof(contentsP));
      }

      var result = new List<FeatureLintData>();

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      if (string.IsNullOrWhiteSpace(doc.Feature.Name))
      {
        result.Add(new FeatureLintData
        {
          Line = 0,
          Column = 0,
          FilePath = contentsP.FilePath,
          Type = FeatureLintDataType.Error,
          Message = NoFeatureTitleMessage
        });
      }

      return result;
    }
  }
}
