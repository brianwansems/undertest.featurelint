using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class NoPeriodsEndingLinesRule : ILintRule
  {
    public int? Order { get; } = null;

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      if (contentsP == null)
      {
        throw new ArgumentNullException(nameof(contentsP));
      }

      var result = new List<FeatureLintData>();

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      if (EndsWithPeriod(doc.Feature.Name))
      {
        result.Add(BuildResultItem(contentsP, doc.Feature, "Feature name"));
      }

      foreach (var scenario in doc.Feature.Children.GatherAllStepsContainers())
      {
        if (EndsWithPeriod(scenario.Name))
        {
          result.Add(BuildResultItem(contentsP, scenario, "Scenario name"));
        }

        result.AddRange(from
                          step
                          in scenario.Steps
                        where
                          EndsWithPeriod(step.Text)
                        select
                          BuildResultItem(contentsP, step, "Step"));
      }

      return result;
    }

    private static FeatureLintData BuildResultItem(FeatureFileDetails fileContents, IHasLocation locationContext, string contextName)
    {
      return new FeatureLintData
      {
        Line = locationContext.Location.Line,
        Column = locationContext.Location.Column,
        FilePath = fileContents.FilePath,
        Message = $"{contextName} should not end in a period",
        Type = FeatureLintDataType.Warning
      };
    }

    private static bool EndsWithPeriod(string input)
    {
      return input.EndsWith(".");
    }
  }
}
