using System;
using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class NoThenInBackgroundsRule : ILintRule
  {
    public int? Order { get; } = null;

    public static string NoThenInBackgroundsMessage = "Background should not contain then statements";

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      if (contentsP == null)
      {
        throw new ArgumentNullException(nameof(contentsP));
      }

      var result = new List<FeatureLintData>();

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      foreach (var scenario in doc.Feature.Children)
      {
        switch (scenario)
        {
          case Background background:
            result.AddRange(from step in background.Steps
                            where step.Keyword.ToLower().Contains("then")
                            select new FeatureLintData
                            {
                              Line = step.Location.Line,
                              Column = step.Location.Column,
                              FilePath = contentsP.FilePath,
                              Type = FeatureLintDataType.Warning,
                              Message = NoThenInBackgroundsMessage
                            });
            break;
        }
      }

      return result;
    }
  }
}
