using System;
using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class ScenarioOutlineShouldHaveAnExampleTableRule : ILintRule
  {
    public static string ScenarioOutlineShouldHaveExampleMessage = "Scenario Outline should have an example table with at least one row";

    public int? Order { get; } = null;

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      if (contentsP == null)
      {
        throw new ArgumentNullException(nameof(contentsP));
      }

      var result = new List<FeatureLintData>();

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      var scenarios = doc.Feature.Children.GatherAllScenarios();
      foreach (var scenario in scenarios)
      {
        if (!scenario.Examples.Any()
            || scenario.Examples.Any(x => x.TableBody == null
                                                  || !x.TableBody.Any()))
        {
          result.Add(new FeatureLintData
          {
            Line = scenario.Location.Line,
            Column = scenario.Location.Column,
            FilePath = contentsP.FilePath,
            Type = FeatureLintDataType.Error,
            Message = ScenarioOutlineShouldHaveExampleMessage
          });
        }
      }

      return result;
    }
  }
}
