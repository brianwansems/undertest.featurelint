using System;
using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class BadWordsInWhenGiveThenRule : ILintRule
  {
    public int? Order { get; } = null;

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      if (contentsP == null)
      {
        throw new ArgumentNullException(nameof(contentsP));
      }

      var result = new List<FeatureLintData>();
      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      foreach (var scenario in doc.Feature.Children)
      {
        switch (scenario)
        {
          case StepsContainer stepsContainer:
          {
            foreach (var step in stepsContainer.Steps)
            {
              if (StepContainsBadWord(step, out string foundWord))
              {
                result.Add(new FeatureLintData
                {
                  Line = step.Location.Line,
                  Column = step.Location.Column,
                  FilePath = contentsP.FilePath,
                  Type = FeatureLintDataType.Warning,
                  Message = $"Bad word found '{foundWord.Trim()}' - on line '{step.Text}'"
                });
              }
            }

            break;
          }
        }
      }

      return result;
    }

    protected virtual bool StepContainsBadWord(Step stepP, out string foundWord)
    {
      foundWord = null;

      // todo - make bad words configurable
      var bad_word_list = new List<string>(new string[]
      {
        "or",
        "and",
        "and/or",
        "if",
        "can",
        "may",
        "need",
        "needs",
        "require",
        "requires",
        "wants",
        "want",
        "might",
        "Lorem ipsum"
      });

      var text = stepP.Text;
      var punctuation = text.Where(char.IsPunctuation).Distinct().ToArray();
      var words = text.Split().Select(x => x.Trim(punctuation)).ToList();
      foreach (var bad_word in bad_word_list)
      {
        if (words.Contains(bad_word))
        {
          foundWord = bad_word;

          return true;
        }
      }

      return false;
    }
  }
}
