using System;
using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class NoDuplicateWhenGivenThenRule : ILintRule
  {
    public int? Order { get; } = null;

    public IList<FeatureLintData> Run(FeatureFileDetails contentsP)
    {
      if (contentsP == null)
      {
        throw new ArgumentNullException(nameof(contentsP));
      }

      const string whenKeyword = "When";
      const string givenKeyword = "Given";
      const string thenKeyword = "Then";

      var result = new List<FeatureLintData>();
      var keywords = new List<string> {whenKeyword, givenKeyword, thenKeyword};
      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      var scenarios = doc.Feature.Children.GatherAllStepsContainers();
      foreach (var scenario in scenarios)
      {
        foreach (var keyword in keywords)
        {
          var duplicates = scenario
                              .Steps
                              .Where(x =>
                                 string.Equals(keyword, x.Keyword.Trim(), StringComparison.InvariantCultureIgnoreCase))
                              .ToList();
          if (duplicates.Count > 1)
          {
            result.Add(MapToFeatureLintData(contentsP, duplicates.First(), keyword));
          }
        }
      }

      return result;
    }

    private static FeatureLintData MapToFeatureLintData(FeatureFileDetails contentsP, Step step, string keyword)
    {
      return new FeatureLintData
      {
        Line = step.Location.Line,
        Column = step.Location.Column,
        FilePath = contentsP.FilePath,
        Message = $"Duplicate {keyword} keyword found. The `And` keyword should be used when duplicates are found.",
        Type = FeatureLintDataType.Error
      };
    }
  }
}
