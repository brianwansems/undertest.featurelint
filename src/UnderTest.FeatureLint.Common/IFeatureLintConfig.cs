using System.Collections.Generic;

namespace UnderTest.FeatureLint.Common
{
  public interface IFeatureLintConfig
  {
    IList<string> FilePaths { get; }
  }
}
