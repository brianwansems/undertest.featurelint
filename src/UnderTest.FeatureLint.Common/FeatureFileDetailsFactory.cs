using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Gherkin;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common
{
  [PublicAPI]
  public class FeatureFileDetailsFactory
  {
    public virtual FeatureFileDetails GetInstance([ItemNotNull] string filePathP)
    {
      if (string.IsNullOrWhiteSpace(filePathP))
      {
        throw new ArgumentNullException(nameof(filePathP));
      }

      var contents = ReadContent(filePathP);
      return new FeatureFileDetails
      {
        Contents = contents,
        FilePath = filePathP,
        Gherkin = (new Parser()).Parse(new StringReader(contents))
      };
    }

    [ExcludeFromCodeCoverage]
    protected virtual string ReadContent(string filePathP)
    {
      return File.ReadAllText(filePathP);
    }
  }
}
