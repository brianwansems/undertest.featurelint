﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace UnderTest.FeatureLint.Common
{
  public class FeatureLintConfig : IFeatureLintConfig
  {
    public FeatureLintConfig(IFeatureLintConfig options)
    {
      if (options == null)
      {
        throw new ArgumentNullException(nameof(options));
      }

      filepaths = options.FilePaths;
    }

    private readonly IList<string> filepaths;

    public IList<string> FilePaths => filepaths;
  }
}
