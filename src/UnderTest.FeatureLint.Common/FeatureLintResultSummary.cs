namespace UnderTest.FeatureLint.Common
{
  public class FeatureLintResultSummary
  {
    public int Errors { get; set; }

    public int Warnings { get; set; }

    public int TotalFindings { get; set; }
  }
}
