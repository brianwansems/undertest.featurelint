using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Gherkin;
using UnderTest.FeatureLint.Common.Extensions;
using UnderTest.FeatureLint.Common.IO;
using UnderTest.FeatureLint.Common.Rules;

using static UnderTest.FeatureLint.Common.IO.FileCollector;

namespace UnderTest.FeatureLint.Common
{
  public class FeatureLintRunner
  {
    public FeatureLintRunner(IFeatureLintConfig configP)
    {
      Config = configP;

      // todo - move this to reflection
      m_Linters.AddRange(new List<ILintRule>{
        new BadWordsInWhenGiveThenRule(),
        new OrderOfWhenGiveThenRule(),
        new FeatureNameRequiredRule(),
        new ScenarioNameRequiredRule(),
        new ScenarioOutlineShouldHaveAnExampleTableRule(),
        new ScenarioOutlineExamplesShouldHaveANameRule(),
        new EnsureScenarioNamesAreUniqueRule(),
        new EnsureFeatureNamesAreUniqueRule(),
        new NoThenInBackgroundsRule(),
        new NoDuplicateWhenGivenThenRule(),
        new NoEmptyFeatureFileRule(),
        new NoDuplicateTagsRule(),
        new NoUpperCaseWordsRule(),
      });
    }

    public IFeatureLintConfig Config { get; }

    private List<ILintRule> m_Linters { get; } = new List<ILintRule>();

    public FeatureLintResult Execute()
    {
      var result = new FeatureLintResult();
      var files = CollectFilesFromGlobs(Directory.GetCurrentDirectory(), Config.FilePaths);
      var valid_files_details = new List<FeatureFileDetails>();
      foreach (var file in files)
      {
        // load the file and check for any errors that cause us to not continue in this file.
        var details = LoadDetails(file, out var failureErrors);
        if (failureErrors.Any())
        {
          result.Findings.AddRange(failureErrors);
          continue;
        }

        valid_files_details.Add(details);
      }

      foreach (var file in valid_files_details)
      {
        file.OtherValidFiles = valid_files_details.Where(x => x.FilePath != file.FilePath).ToList();
        // if we get here we have a valid gherkin file which we can now lint.
        var findings = RunLinters(file);
        if (findings.Any())
        {
          result.Findings.AddRange(findings);
        }
      }

      result.UpdateSummary();

      return result;
    }

    private FeatureFileDetails LoadDetails(string fileP, out IList<FeatureLintData> failureErrorsP)
    {
      failureErrorsP = new List<FeatureLintData>();
      try
      {
        return new FeatureFileDetailsFactory().GetInstance(fileP);
      }
      catch (CompositeParserException e)
      {
        foreach (var error in e.Errors)
        {
          failureErrorsP.Add(new FeatureLintData
          {
            Type = FeatureLintDataType.Error,
            Line = error.Location.Line,
            Column = error.Location.Column,
            FilePath = fileP,
            Message = error.Message
          });
        }
      }
      catch (ParserException e)
      {
        failureErrorsP.Add(new FeatureLintData
        {
          Type = FeatureLintDataType.Error,
          Line = e.Location.Line,
          Column = e.Location.Column,
          FilePath = fileP,
          Message = e.Message
        });
      }

      return new FeatureFileDetails
      {
        FilePath = fileP,
        Contents = File.ReadAllText(fileP)
      };
    }

    private IList<FeatureLintData> RunLinters(FeatureFileDetails fileDetailsP)
    {
      var result = new List<FeatureLintData>();

      foreach (var linter in m_Linters)
      {
        result.AddRange(linter.Run(fileDetailsP));
      }

      return result;
    }
  }
}
