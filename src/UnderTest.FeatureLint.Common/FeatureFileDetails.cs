using System.Collections.Generic;
using Gherkin.Ast;

namespace UnderTest.FeatureLint.Common
{
  public class FeatureFileDetails
  {
    public string FilePath { get; set; }

    public string Contents { get; set; }

    public GherkinDocument Gherkin { get; set; }

    public List<FeatureFileDetails> OtherValidFiles { get; set; } = new List<FeatureFileDetails>();
  }
}
