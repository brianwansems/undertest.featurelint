Feature: Find bad words

Certain words and phrases should be flagged as dangerous when writing gherkin.

Scenario Outline: Team member uses a bad word
  Given a feature file
    And the feature file contains a step with the phrase <bad-phrase>
  When I lint the feature file
  Then I should be warned about using the phrase <bad-phrase>

  Examples: Bad phrases in steps
   | bad-phrase |
   | and        |
   | or         |
   | and/or     |
