using JetBrains.Annotations;

using TechTalk.SpecFlow;

using FluentAssertions;

using UnderTest.FeatureLint.AcceptanceTests.Models;
using UnderTest.FeatureLint.Common;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.Steps
{
  [Binding]
  [UsedImplicitly]
  public class RuleBindings
  {
    public RuleBindings(FeatureTestContext contextP)
    {
      context = contextP;
    }

    private FeatureTestContext context;

    [Given(@"a feature file")]
    [UsedImplicitly]
    public void GivenAFeatureFile()
    {
      // context handles setting up a feature.
    }

    [Given(@"the feature file contains a step with the phrase (.*)")]
    [UsedImplicitly]
    public void GivenTheFeatureFileContainsAStepWithThePhraseWildcard(string word)
    {
      context.ReplaceWordPlaceholder(word);
    }

    [When(@"I lint the feature file")]
    [UsedImplicitly]
    public void WhenILintTheFeatureFile()
    {
      context.Result = new BadWordsInWhenGiveThenRule()
                                  .Run(new InMemoryFeatureFileDetailsFactory
                                    {
                                      FeatureContent = context.Content
                                    }.GetInstance());
    }

    [Then(@"I should be warned about using the phrase (.*)")]
    [UsedImplicitly]
    public void ThenIShouldBeWarnedAboutUsingThePhraseAnd(string word)
    {
      context.Result.Count.Should().Be(1);
    }
  }
}
