namespace UnderTest.FeatureLint.AcceptanceTests.Models
{
  using System.Collections.Generic;

  using JetBrains.Annotations;

  using Common;

  [PublicAPI]
  public class FeatureTestContext
  {
    public FeatureTestContext()
    {
      Content = FeatureWithOneValidScenarioWithPlaceholders;
    }

    private const string FeatureNamePlaceholder = "FeatureNamePlaceholder";

    private const string ScenarioNamePlaceholder = "ScenarioPlaceHolder";

    private const string StepWordPlaceholder = "WordPlaceHolder";

    private readonly string FeatureWithOneValidScenarioWithPlaceholders = $@"Feature: {FeatureNamePlaceholder}

Scenario: {ScenarioNamePlaceholder}
  Given all of this is true {StepWordPlaceholder} with other stuff
  When something happens
  Then these new things are true";

    public string Content { get; set; }

    public IList<FeatureLintData> Result { get; set; }

    public FeatureTestContext ReplaceWordPlaceholder(string word)
    {
      Content = Content.Replace(StepWordPlaceholder, word);

      return this;
    }
  }
}
