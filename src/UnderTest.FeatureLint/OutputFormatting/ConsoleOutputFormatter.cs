using System.Drawing;
using System.Linq;
using Colorful;
using UnderTest.FeatureLint.Common;
using UnderTest.FeatureLint.Common.Extensions;
using UnderTest.FeatureLint.Common.OutputFormatting;

namespace UnderTest.FeatureLint.OutputFormatting
{
  public class ConsoleOutputFormatter : IOutputFormatter
  {
    public string BuildErrorResult(FeatureLintResult result)
    {
      Console.WriteLine();
      Console.WriteLine();

      var file_findings = result.GroupByFilename();

      foreach (var item in file_findings)
      {
        Console.WriteLine(item.FilePath, Color.White);
        var ordered_findings = item.Findings.OrderBy(x => x.Line);
        foreach (var finding in ordered_findings)
        {
          var output_coloring = new[]
          {
            new Formatter(finding.Line, Color.White),
            new Formatter(finding.Column, Color.White),
            new Formatter(finding.Type, Color.DarkRed),
            new Formatter(finding.Message, Color.Gray),
          };

          Console.WriteLineFormatted(" {0}:{1} {2} - {3}", Color.White, output_coloring);
        }
      }

      Console.WriteLine();
      Console.WriteLine($" X {result.Summary.TotalFindings} problems ({result.Summary.Errors} errors, {result.Summary.Warnings} warnings)", Color.DarkRed);

      return string.Empty;
    }

    public string BuildSuccessResult()
    {
      return $"All good in the hood";
    }
  }
}
