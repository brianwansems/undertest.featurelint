﻿using System.Linq;
using CommandLine;
using JetBrains.Annotations;
using UnderTest.FeatureLint.Common;
using System.Drawing;
using Colorful;
using UnderTest.FeatureLint.Common.OutputFormatting;
using UnderTest.FeatureLint.OutputFormatting;
using Console = Colorful.Console;

namespace UnderTest.FeatureLint
{
  [PublicAPI]
  public class Program
  {
    static int Main(string[] args)
    {
      Console.OutputEncoding = System.Text.Encoding.UTF8;

      return Parser.Default.ParseArguments<Options>(args)
        .MapResult(
          options => (int)RunAndReturnExitCode(options),
          _ =>
          {
            if(_.Any(e => e is VersionRequestedError))
            {
              return (int) AppExitCode.Success;
            }

            return (int) AppExitCode.InvalidArguments;
          });
    }

    private static AppExitCode RunAndReturnExitCode(Options options)
    {
      var result = new FeatureLintRunner(options)
                      .Execute();

      var output = GetOutputFormatterFromOptions(options);
      if (!result.Findings.Any())
      {
        Console.WriteLine(output.BuildSuccessResult());
        return AppExitCode.Success;
      }

      Console.WriteLine(output.BuildErrorResult(result));
      return AppExitCode.ErrorsFound;
    }

    private static IOutputFormatter GetOutputFormatterFromOptions(Options options)
    {
      if (options.JsonOutput)
      {
        return new JsonOutputFormatter();
      }

      return new ConsoleOutputFormatter();
    }
  }
}
