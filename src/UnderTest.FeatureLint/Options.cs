using System.Collections.Generic;
using CommandLine;
using JetBrains.Annotations;
using UnderTest.FeatureLint.Common;

namespace UnderTest.FeatureLint
{
  [PublicAPI]
  class Options : IFeatureLintConfig
  {
    [Value(0, MetaName = "filepaths", Required = false, Default = new string[] {"**/*.feature" }, HelpText = "Input filepath(s).  Supports globs, absolute and relative paths.")]
    public IList<string> FilePaths { get; set; }

    [Option("json", Default = false, HelpText = "Display lint results as JSON")]
    public bool JsonOutput { get; set; }
  }
}
