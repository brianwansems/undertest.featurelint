# Change Log

All notable changes to UnderTest.FeatureLint will be documented in this file.

## [Unreleased]
### Added
* Added new rule for NoUpperCaseWords #55
### Changed
* Updated the error message for the `ScenarioOutlineShouldHaveAnExampleTableRule` rule #74
* `--version` now outputs a success exitcode
* upgraded to Gherkin 0.6.0 #80

## 0.2.0 - Apr 7, 2019
### Added
- New rule for duplicate tags #69
- Support for output JSON from CLI #63
- New bad word `Lorem ipsum` #54
- New rule `Check for duplicate step keywords` #53
### Changed
- Updated to latest Nuke.Common for build pipeline #70  
- Fix glob when passed a folder #64

## 0.1.2 - Mar 4, 2019
### Added
- New word `Can` to bad words
### Changed

## 0.1.2 - Feb 15, 2019
### Changed
- Fix NullReferenceException on EnsureFeatureNamesAreUniqueRule

## 0.1.1 - Feb 15, 2019
### Added
- No Empty Feature File Rule

### Changed
- Fix NullReferenceException on Empty Feature File

## 0.1.0 - Jan 22, 2019

- Initial release
